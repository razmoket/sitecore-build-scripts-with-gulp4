# Sitecore Build Scripts with Gulp4

# General 
We are using these tools, technologies and modules:
- [NodeJS 14.16.0](https://nodejs.org/en/)
    - Can use NVM to install nodejs. See more informations below.
- [Gulp 4.0.2](https://gulpjs.com/)
- [NPM 6.4.11](https://www.npmjs.com/)

# This project was tested with :
- Sitecore 8.2 U7 and Sitecore 9.0.1
- .Net Framework 4.5.2
    - Make sure you have correctly installed the necessary package within the IIS installation.
- Visual Studio 2019
    - Comes with msbuild 16.0

## Getting Started
The build scripts are using nodejs, npm and gulp.
For nodejs usage, I highly recommend using nvm (node version manager) as it is easier to handle node js updates.

When working with Sitecore, you might want to make sure you have these installed :
* [IIS](https://enterprise.arcgis.com/en/web-adaptor/latest/install/iis/enable-iis-10-components-server.htm)
* [Chocolatey](https://chocolatey.org/install)
    * You can then use choco to install any package :
        * `choco i vscode`
* MsDeploy
    * Use IIS Web Platform installer to install and to confirm it is installed
* [NVM](https://github.com/coreybutler/nvm-windows)
* Nodejs
    * Use NVM to install the LTS.
    * When install, you can easily install any nodejs version :
        * `nvm i nodeJsVersion`
    * You can then see all of your nodejs version ;
        * `nvm list`
    * Pick the nodejs version you want to use :
        * `nvm use yourNodeJsVersion`

Gulp will be installed by running `npm i`

## Overview of the build architecture
[Gulp](https://gulpjs.com/)
* Gulp is being used to define Tasks. 
    * You can see them there : __*./gulp/tasks*__
* There is a configuration file where we definie variables such as path, artifacts and solution name. 
    * You can see it here : __*./gulp/config/config.json*__
    * This is also where you want to update all the values to adapt the script to your solution

[Node.js](https://nodejs.org/en/)
* It is a JS event-driven server.
    * We use it in parallel to import, export and define task. 
    * It is omnipresent in almost all gulp tasks.

[NPM](https://www.npmjs.com/)
* It is the node package manager.
    * We use it to manage node js packages
        * You can see the packages that are being installed here : __*./package.json*__
        * to install an npm package and save it for dev depedencies, run this : `npm i --save-dev yourPackage`

## How to build the solution
There are many build scripts available, they are define within the __*./package.json*__ file.

Every script have and should have a script definition (prefixed by __*?*__),
It is used to show definition when running `npm run info`

Run this script to show it all : `npm run info`
Run this script to build the whole solution : `npm run build`
Run this script to build the backend : `npm run build:backend`
- Behind the scene, it uses msbuild to build backend solution.
    - Find it here : __*./gulp/tasks/_utils/publishStream*__
    - Here is a list of the available [msbuild switches](https://docs.microsoft.com/en-us/visualstudio/msbuild/msbuild-command-line-reference?view=vs-2019)
    - Here is the [msbuild plugin being used](https://www.npmjs.com/package/msbuild) 
Run this script to build the frontent : `npm run build:frontend`
    - Most of the frondend task are about compression and copying.