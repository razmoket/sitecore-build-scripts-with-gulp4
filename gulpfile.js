global.config = require('./gulp/config/config.json');

require('events').EventEmitter.prototype._maxListeners = 1000;

require('require-dir')('./gulp/tasks/_utils');
require('require-dir')('./gulp/tasks/build');
require('require-dir')('./gulp/tasks/clean');
require('require-dir')('./gulp/tasks/compress');
require('require-dir')('./gulp/tasks/copy');
require('require-dir')('./gulp/tasks/publish');
require('require-dir')('./gulp/tasks/restore');