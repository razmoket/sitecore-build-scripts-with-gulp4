const {src, dest, task} = require("gulp");
const stripDebug = require('gulp-strip-debug');
const shell = require('gulp-shell');
const logger = require('node-color-log');
const uglify = require('gulp-uglify');

const jsSrc = config.path.solution.js;
const jsDest = config.path.artifact.js;

function compressJs(callback) {
  logger.color('yellow').log(`Removing console.log() and uglifying js from ${config.path.solution.js} to: ${config.path.artifact.js}`);

    src(jsSrc)
    .pipe(stripDebug())
    .pipe(uglify())
    .pipe(dest(`${jsDest}`))
    .on('end', function () {
        callback();
    });
}

const Js = task('compress:js', compressJs);
exports.Js = Js;