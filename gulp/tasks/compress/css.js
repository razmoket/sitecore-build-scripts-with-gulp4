const {src, dest, task} = require("gulp");
const shell = require('gulp-shell');
const logger = require('node-color-log');
const cleanCSS = require('gulp-clean-css');
const cssSrc = config.path.solution.css;
const cssDest = config.path.artifact.css;

function compressCss(callback) {
    logger.color('yellow').log(`Compressing css from ${config.path.solution.css} to: ${config.path.artifact.css}`);
    
    src(cssSrc)
    .pipe(cleanCSS({debug: true}, (details) => {
        console.log(`css file name: ${details.name}, optimization(bytes) = ${(details.stats.originalSize - details.stats.minifiedSize)}`);
    }))
    .pipe(dest(`${cssDest}`))
    .on('end', function () {
        callback();
    });
};

const Css = task('compress:css', compressCss);
exports.Css = Css;