const { 
    src, 
    dest, 
    task } = require( 'gulp' );

const strip = require('gulp-strip-comments');
const logger = require('node-color-log');

function cleanWebConfigTask(callback){
    logger.color('yellow').log(`Removing comments from ${config.path.solution.webConfig} and send to: ${config.path.artifact.website}`);
    return src(config.path.solution.webConfig, { allowEmpty: true})
        .pipe(strip())
        .pipe(dest(config.path.artifact.website))
        .on('end', function () { callback(); });
}

const cleanWebConfigs = task('compress:webConfig', cleanWebConfigTask);
exports.cleanWebConfigs = cleanWebConfigs;