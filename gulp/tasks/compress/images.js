const {
    src,
    dest,
    task
} = require("gulp");

const logger = require('node-color-log');
const imagemin = require('gulp-imagemin');

function compressImagemin(callback) {
    const imgSrc = config.path.solution.images + '/**/*';
    const imgDest = config.path.artifact.images;

    logger.color('yellow').log(`Optimise images from ${imgSrc} in: ${imgDest}`);

    src(imgSrc)
        .pipe(imagemin())
        .pipe(dest(`${imgDest}`))
        .on('end', function () {
            callback();
        });
}

const compressImages = task('compress:images', compressImagemin);
exports.compressImages = compressImages;