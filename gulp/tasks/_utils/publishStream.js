const logger = require('node-color-log');
const _msbuild = require('msbuild');

function publishStream(dest, filePath) {

    var msbuild = new _msbuild();

    const targets = ['Clean', 'Build'];
    logger.color('yellow').log('Publish stream');

    var overrideParams = [];
    overrideParams.push(`/t:${targets}`,
        `/v:${config.solution.buildVerbosity}`,
        '/nologo',
        `/m:${config.solution.buildMaxCpuCount}`,
        '/nr:False',
        `/p:Configuration=${config.solution.buildConfiguration}`,
        `/p:Platform=${config.solution.buildPlatform}`,
        '/p:DeployOnBuild=true',
        '/p:DeployDefaultTarget=WebPublish',
        '/p:WebPublishMethod=FileSystem',
        '/p:DeleteExistingFiles=false',
        `/p:publishUrl=${dest}`,
        '/p:_FindDependencies=true');

    msbuild.sourcePath = filePath;
    msbuild.config('overrideParams', overrideParams);
    msbuild.version = `${config.solution.buildToolsVersion}`;
    msbuild.build();
};

exports.publishStream = publishStream;