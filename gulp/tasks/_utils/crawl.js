const { task } = require( 'gulp' );
const Crawler = require("js-crawler");
const logger = require('node-color-log');
const fs = require('fs');

const args = require('minimist')(process.argv.slice(2));

let env = "local";

if (args.env) {
  env = args.env;
}

const websites = [
  `https://${env}.domainA.com/`,
  `https://${env}.domainB.com/`
]

let depthValue = 1;

if (args.depth) {
  depthValue = args.depth;
}

const crawler = new Crawler().configure({
  ignoreRelative: true, depth: depthValue,
  maxRequestsPerSecond: 1,
  maxConcurrentRequests: 1
});

var currentDate = new Date()
var currentFormattedDate = currentDate.toISOString().slice(0, 10);
var currentHour = currentDate.getHours();
var currentMinute = currentDate.getMinutes();

function crawlWebsites(callback) {
    return websites.forEach(website => getWebsiteStatus(website, callback));
};

function getWebsiteStatus(website, callback) {
  crawler.crawl({
    url: website,
    success: function(page) {
      logger.color('blue').log(`Url crawled : ${page.url}`);
      logger.color('green').log(`Url status : ${page.status}`);
    },
    failure: function(page) {
      logger.color('red').log(`Url crawled : ${page.url}`);
      logger.color('red').log(`Url status : ${page.status}`);
      writeErr(`${page.url}:${page.status}`)
    },
    finished: function(crawledUrls) {
      crawler.isStopped = true;
      return callback();
    }
  });
}

function writeErr(website){
  
  if (!fs.existsSync(`./crawlReport/${currentFormattedDate}`)) {
    fs.mkdirSync(`./crawlReport/${currentFormattedDate}`, {
      recursive: true
    });
  }

  if (!fs.existsSync(`./crawlReport/${currentFormattedDate}/${currentHour}`)) {
    fs.mkdirSync(`./crawlReport/${currentFormattedDate}/${currentHour}`, {
      recursive: true
    });
  }

  if (!fs.existsSync(`./crawlReport/${currentFormattedDate}/${currentHour}/${currentMinute}.txt`)) {
    fs.writeFile(`./crawlReport/${currentFormattedDate}/${currentHour}/${currentMinute}.txt`, website, function (err) {
      if (err) {
        return console.log(err);
      }
    });
  }
  else{
    fs.appendFile(`./crawlReport/${currentFormattedDate}/${currentHour}/${currentMinute}.txt`, `\r\n${website}`, function (err) {
      if (err) {
        return console.log(err);
      }
    });
  }
}

const websitesCrawler = task('crawl', crawlWebsites);
exports.websitesCrawler = websitesCrawler;