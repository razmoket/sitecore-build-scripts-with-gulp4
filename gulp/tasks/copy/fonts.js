const { src, dest, task } = require( 'gulp');
const logger = require('node-color-log');

function copyFontsTask(callback) {
    let rootFonts = config.path.solution.fonts;
    let artifactFonts = config.path.artifact.fonts;

    logger.color('yellow').log(`Copy ${rootFonts} into ${artifactFonts}`);

    src(rootFonts)
        .pipe(dest(artifactFonts), 
        callback());
};

const copyFonts = task('copy:fonts', copyFontsTask);
exports.copyFonts = copyFonts;