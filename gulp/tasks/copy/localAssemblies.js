const { src, dest, task } = require( 'gulp' );
const foreach = require("gulp-foreach");
const logger = require('node-color-log');
var rename = require('gulp-rename');

function localAssemblies(callback) {
    const binSrc = config.path.artifact.sitecoreLibraries;
    const root = config.path.solution.root;

    const websiteSrc = root + "**/Website/bin/**/*";

    return src(root, { base: root })
        .pipe(foreach(function (stream) {
            logger.color('yellow').log(`Copying site assemblies from ${websiteSrc} to ${binSrc}`);
            src(websiteSrc, {nodir: true})
            .pipe(rename({dirname: ''}))
            .pipe(dest(binSrc));
            return stream;
        }));
};

const copyLocalAssemblies = task('copy:localAssemblies', localAssemblies);
exports.copyLocalAssemblies = copyLocalAssemblies;