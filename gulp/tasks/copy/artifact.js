const { src, dest, task } = require( 'gulp' );
const logger = require('node-color-log');

function copyArtifactInstance(callback) {
    const artifactData = config.path.artifact.website + "/**/*";
    const destination = config.path.server.website;

    logger.color('green').log(`Copy artifact from ${artifactData} to: ${destination}`);

    return src(artifactData)
			.pipe(dest(destination))
			.on('end', function () { 
			    logger.color('green').log(`Copy to: ${destination}`);
				callback(); 
			});
};

const copyArtifact = task('copy:artifact', copyArtifactInstance);
exports.copyArtifact = copyArtifact;