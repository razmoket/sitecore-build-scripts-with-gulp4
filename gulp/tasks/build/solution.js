const {
    src,
    task
} = require("gulp");
const shell = require('gulp-shell');

function build() {
    return src('./', {
            read: false
        })
        .pipe(shell([
            'gulp clean:artifact'
        ]))
        .pipe(shell([
            'gulp compress:images'
        ]))
        .pipe(shell([
            'gulp compress:css'
        ]))
        .pipe(shell([
            'gulp compress:js'
        ]))
        .pipe(shell([
            'gulp copy:fonts'
        ]))
        .pipe(shell([
            'gulp restore:nuget'
        ]))
        .pipe(shell([
            'gulp publish:project'
        ]))
        .pipe(shell([
            'gulp copy:localAssemblies'
        ]))
        .pipe(shell([
            'gulp copy:artifact'
        ]))
};

const runBuild = task('build:solution', build);
exports.runBuild = runBuild;