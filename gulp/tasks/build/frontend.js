const {
    src,
    task
} = require("gulp");
const shell = require('gulp-shell');

function build() {
    return src('./', {
            read: false
        })
        .pipe(shell([
            'gulp clean:images'
        ]))
        .pipe(shell([
            'gulp clean:css'
        ]))
        .pipe(shell([
            'gulp clean:js'
        ]))
        .pipe(shell([
            'gulp clean:fonts'
        ]))
        .pipe(shell([
            'gulp compress:images'
        ]))
        .pipe(shell([
            'gulp compress:css'
        ]))
        .pipe(shell([
            'gulp compress:js'
        ]))
        .pipe(shell([
            'gulp copy:fonts'
        ]))
        .pipe(shell([
            'gulp clean:logs'
        ]))
};

const buildFrontend = task('build:frontend', build);
exports.buildFrontend = buildFrontend;