const { 
    src, 
    task 
} = require("gulp");
const shell = require('gulp-shell');

function build() {
    return src('./', {
            read: false
        })
        .pipe(shell([
            'gulp restore:nuget'
        ]))
        .pipe(shell([
            'gulp clean:bin'
        ]))
        .pipe(shell([
            'gulp publish:project'
        ]))
        .pipe(shell([
            'gulp copy:localAssemblies'
        ]))
        .pipe(shell([
            'gulp copy:artifact'
        ]));
};

const buildBackend = task('build:backend', build);
exports.buildBackend = buildBackend;