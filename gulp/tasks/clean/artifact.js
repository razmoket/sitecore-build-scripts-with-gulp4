const { src, task } = require( 'gulp' );
const clean = require("gulp-clean");
const logger = require('node-color-log');

function cleanArtifactTask() {
    const root = config.path.artifact.root;

    logger.color('yellow').log(`Clean Artifact`);

    return src(root,{allowEmpty: true},{read: false}).pipe(clean({force:true}));
};

const cleanArtifactFolder = task('clean:artifact', cleanArtifactTask);
exports.cleanArtifactFolder = cleanArtifactFolder;