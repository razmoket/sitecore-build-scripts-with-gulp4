const { 
    src, 
    dest, 
    task } = require( 'gulp' );

const logger = require('node-color-log');
const stripDebug = require('gulp-strip-debug');

function cleanLogsTask(callback){
    logger.color('green').log(`Remove console.log()`);

    return src(config.path.artifact.js, { allowEmpty: true})
        .pipe(stripDebug())
        .pipe(dest(config.path.artifact.js))
        .on('end', function () { callback(); });
}

const cleanlogs = task('clean:logs', cleanLogsTask);
exports.cleanlogs = cleanlogs;