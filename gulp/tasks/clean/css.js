const { src, task } = require( 'gulp' );
const clean = require("gulp-clean");
const logger = require('node-color-log');

function cleanCssTask() {
    const root = config.path.artifact.css;

    logger.color('yellow').log(`Clean css`);

    return src(root,{allowEmpty: true},{read: false}).pipe(clean({force:true}));
};

const cleanCssFolder = task('clean:css', cleanCssTask);
exports.cleanCssFolder = cleanCssFolder;