const { src, task } = require( 'gulp' );
const clean = require("gulp-clean");
const logger = require('node-color-log');

function cleanFontsTask() {
    const root = config.path.artifact.fonts;

    logger.color('yellow').log(`Clean fonts`);

    return src(root,{allowEmpty: true},{read: false}).pipe(clean({force:true}));
};

const cleanFontsFolder = task('clean:fonts', cleanFontsTask);
exports.cleanFontsFolder = cleanFontsFolder;