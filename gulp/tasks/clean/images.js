const { src, task } = require( 'gulp' );
const clean = require("gulp-clean");
const logger = require('node-color-log');

function cleanImagesTask() {
    const root = config.path.artifact.images;

    logger.color('yellow').log(`Clean images`);

    return src(root,{allowEmpty: true},{read: false}).pipe(clean({force:true}));
};

const cleanImagesFolder = task('clean:images', cleanImagesTask);
exports.cleanImagesFolder = cleanImagesFolder;