const { src, task } = require( 'gulp' );
const clean = require("gulp-clean");
const logger = require('node-color-log');

function cleanBinTask() {
    const root = config.path.artifact.sitecoreLibraries;

    logger.color('yellow').log(`Clean bin`);

    return src(root,{allowEmpty: true},{read: false}).pipe(clean({force:true}));
};

const cleanBinFolder = task('clean:bin', cleanBinTask);
exports.cleanBinFolder = cleanBinFolder;