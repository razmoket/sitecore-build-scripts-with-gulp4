const { src, task } = require( 'gulp' );
const clean = require("gulp-clean");
const logger = require('node-color-log');

function cleanJsTask() {
    const root = config.path.artifact.js;

    logger.color('yellow').log(`Clean js`);

    return src(root,{allowEmpty: true},{read: false}).pipe(clean({force:true}));
};

const cleanJsFolder = task('clean:js', cleanJsTask);
exports.cleanJsFolder = cleanJsFolder;