const {
    task
} = require("gulp");
const logger = require('node-color-log');


function publishProjectTask() {
    const publishStream = require('../_utils/publishStream').publishStream;
    
    let dest = config.path.artifact.website;
    logger.color('green').log(`Copying to: ${dest}`);

    let path = config.path.solution.root;
    logger.color('green').log(`Publishing from ${path} `);

    return publishStream(dest, path);
};

const publishProject = task('publish:project', function (done) {
    publishProjectTask();
    done();
});

exports.publishProject = publishProject;