const { src, task } = require( 'gulp' );
const shell = require('gulp-shell');

function restore(callback) {
    return src(config.path.solution.sln)
        .pipe(shell([
            'nuget restore'
        ]));
}

const restoreNuget = task('restore:nuget', restore);
exports.restoreNuget = restoreNuget;